import React from 'react';
import {Button, TextInput, View} from 'react-native';

export class NoteEdit extends React.Component {
  constructor (props) {
    super(props)
    this.state = {text:''}
    console.log('Note - constructor')
  }

  static getDerivedStateFromProps () {
    console.log('Note - getDerivedStateFromProps')
  }

  handleOnPress(){
    this.props.onSubmit({text:this.state.text})
    this.setState({text:''})
  }

  render () {
    return (
      <View>
        <TextInput
          style={{height: 40, width: 300}}
          placeholder="Enter text"
          value={this.state.text}
          onChangeText={(text) => this.setState({text})}
        />
        <Button
          onPress={()=>this.handleOnPress()}
          title="Submit"
        />
      </View>
    )
  }

  componentDidMount () {
    console.log('Note - componentDidMount')
  }

  componentDidUpdate () {
    console.log('Note - componentDidUpdate')
  }

  componentWillUnmount () {
    console.log('Note - componentWillUnmount')
  }
}

export default NoteEdit;