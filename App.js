import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Note } from './Note'
import {NoteEdit} from './NoteEdit';

const notes = [{id:1,text:'React'},{id:2,text:'React Native'}];

export default class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0,
      notes: notes
    }
    console.log('App - constructor')
  }

  static getDerivedStateFromProps () {
    console.log('App - getDerivedStateFromProps')
  }

  render () {
    return (
      <View style={styles.container}>
        <NoteEdit onSubmit={(note)=>this.handleOnSubmit(note) }></NoteEdit>
        {
          this.state.notes.map(note=><Note key={note.id} note={note}/>)
        }
        <Note note={{text:this.state.counter}} />
      </View>
    )
  }

  componentDidMount () {
    console.log('App - componentDidMount')
    this.counterIntervalId = setInterval(() => {
      this.setState({counter: this.state.counter+1})
    }, 1000)
  }

  componentDidUpdate () {
    console.log('App - componentDidUpdate')
  }

  componentWillUnmount () {
    console.log('App - componentWillUnmount')
    clearInterval(this.counterIntervalId)
  }

  handleOnSubmit(note){
    this.setState({notes:this.state.notes.concat({id:notes.length+1,text:note.text})})
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
