import React from 'react'
import { Text } from 'react-native'

export class Note extends React.Component {
  constructor (props) {
    super(props)
    console.log('Note - constructor')
  }

  static getDerivedStateFromProps () {
    console.log('Note - getDerivedStateFromProps')
  }

  render () {
    return (
      <Text>
        {this.props.note.text}
      </Text>
    )
  }

  componentDidMount () {
    console.log('Note - componentDidMount')
  }

  componentDidUpdate () {
    console.log('Note - componentDidUpdate')
  }

  componentWillUnmount () {
    console.log('Note - componentWillUnmount')
  }
}

export default Note;